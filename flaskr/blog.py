from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, current_app
)
from werkzeug.exceptions import abort

import sys

from flaskr.db import get_db

bp = Blueprint('blog', __name__)

@bp.route('/', methods=('GET', 'POST')) # routea index a /
def index():
    db = get_db()
    todos = db.execute(
        'SELECT id, title, body, created, completed, modified FROM todo t' #JOIN user u ON p.author_id = u.id'
        ' WHERE completed = 0'
        ' ORDER BY modified DESC'
    ).fetchall()
    dones = db.execute(
        'SELECT id, title, body, created, completed, modified FROM todo' #JOIN user u ON p.author_id = u.id'
        ' WHERE completed = 1'
        ' ORDER BY modified DESC'
    )
    if request.method == "POST":
        print(request.form['done'])
        button_id = request.form['done']
        
        if button_id:
            acValue = db.execute(
                'SELECT id, completed FROM todo'
                ' WHERE id = ?',
                (button_id)
                ).fetchone()
            if acValue['completed'] == 1:
                newValue = 0
            else:
                newValue = 1
            db.execute(
                'UPDATE todo SET completed = ?'
                ' WHERE id = ?',
                (newValue, button_id)
            )
            db.commit()
            return redirect(url_for('index'))

    return render_template('blog/index.html', todos=todos, dones=dones)

@bp.route('/create', methods=('GET', 'POST'))
def create():
    current_app.logger.info("CREATING")
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO todo (title, body)'
                ' VALUES (?, ?)',
                (title, body)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')

def get_todo(id):
    post = get_db().execute(
        'SELECT t.id, title, body, created, modified'
        ' FROM todo t'
        ' WHERE t.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")

    return post

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
def update(id):
    todo = get_todo(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE todo SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', todo=todo)


@bp.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    get_todo(id)
    db = get_db()
    db.execute('DELETE FROM todo WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))
